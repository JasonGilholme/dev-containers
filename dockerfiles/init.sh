# TODO: make docker group

groupadd -g $EXT_GID $EXT_GNAME
echo "$EXT_UNAME:x:$EXT_UID:$EXT_GID:$EXT_UNAME,,,:/home/developer:/bin/bash" >> /etc/passwd
echo "$EXT_UNAME:x:$EXT_GID:" >> /etc/group
echo "$EXT_UNAME ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/$EXT_UNAME
chmod 0440 /etc/sudoers.d/$EXT_UNAME
chown $EXT_UID:$EXT_GID -R /home/developer
pwconv
su -p -l $EXT_UNAME
