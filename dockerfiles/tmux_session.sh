tmux \
    new -s dev -n Editor \; \
    send-keys 'vim' C-m \; \
    split-window -h \; \
    resize-pane -x 70 \; \
    split-window -v\; \
    select-pane -U \; \
    resize-pane -y 20 \; \
    send-keys 'watch todolist list by p' C-m \; \
    select-pane -L \; \
    neww -n Console \; \
    split-window -h \; \
    select-pane -L \; \
    neww -n System \; \
    send-keys 'glances' C-m \; \
    split-window -h \; \
    send-keys 'neofetch' C-m \; \
    split-window -v \; \
    select-pane -U \; \
    resize-pane -y 25 \; \
    resize-pane -x 80 \; \
    select-pane -D \; \
    select-window -t 1
